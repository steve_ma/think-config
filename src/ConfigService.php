<?php
namespace stevema\config;

use think\facade\Config;
use think\Service as BaseService;

class ConfigService extends BaseService
{
    public function register()
    {
        $this->app->bind('smconfig', ConfigManage::class);
    }
    public function boot()
    {

    }
}