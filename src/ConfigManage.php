<?php
namespace stevema\config;

use Exception;
use think\facade\Cache;
use think\facade\Config;

class ConfigManage {

    public function getModel(){
        $modelName = Config::get('smconfig.model');
        if(empty($modelName)) throw new Exception("config/smconfig.php 没有定义model");
        return $modelName;
    }

    /**
     * @param string $type
     * @param string|null $name
     * @param mixed $value
     * @param mixed $source
     * @return mixed|string
     * @throws Exception
     */
    public function set(string $type, string $name = null, mixed $value = '', mixed $source = 'default')
    {
        $cacheKey = 'config' . ':' . $source. ':' . $type . '-' . $name;
        Cache::delete($cacheKey);
        $original = $value;
        $update_time = time();
        if (is_array($value)) {
            $value = json_encode($value, true);
        }
        $modelName = $this->getModel();
        $data = $modelName::where(['type' => $type, 'name' => $name, 'source' => $source])->find();

        if (empty($data)) {
            $modelName::create([
                'type' => $type,
                'name' => $name,
                'value' => $value,
                'source' => $source
            ]);
        } else {
            $modelName::update([
                'value' => $value,
                'update_time' => $update_time
            ], ['type' => $type, 'name' => $name, 'source' => $source]);
        }
        return $original;
    }


    /**
     * @param string $type
     * @param string|null $name
     * @param $defaultValue
     * @param mixed $source
     * @return mixed|null
     * @throws Exception
     */
    public function get(string $type, string $name = null, $defaultValue = null, mixed $source = 'default')
    {
        //有缓存取缓存
        $cacheKey = 'config' . ':' . $source. ':' . $type . '-' . $name;
        $result = Cache::get($cacheKey);
        $value = $result['smconfig'] ?? null;
        if ($value !== null) {
            return $value;
        }

        $modelName = $this->getModel();
        //单项配置
        if ($name) {
            $result = $modelName::where([
                'type' => $type,
                'name' => $name,
                'source' => $source
            ])->value('value');

            //数组配置需要自动转换
            $json = json_decode($result, true);
            if (json_last_error() === JSON_ERROR_NONE) {
                $result = $json;
            }
            //获取调用默认配置
            if ($result === NULL) {
                $result = $defaultValue;
            }
            //获取系统配置文件的配置
            if ($result === NULL) {
                $result = Config::get('smconfig.default.' . $type . '.' . $name);
            }

            Cache::set($cacheKey, ['smconfig' => $result]);
            return $result;
        }

        //多项配置
        $data = $modelName::where([
            'type' => $type,
            'source' => $source
        ])->column('value', 'name');

        if (is_array($data)) {
            foreach ($data as $k => $v) {
                $json = json_decode($v, true);
                if (json_last_error() === JSON_ERROR_NONE) {
                    $data[$k] = $json;
                }
            }
        }
        if ($data === []) {
            $data = $defaultValue;
        }
        if ($data === NULL) {
            $data = Config::get('smconfig.default.' . $type . '.' . $name);
        }

        Cache::set($cacheKey, ['smconfig' => $data]);
        return $data;

    }
}