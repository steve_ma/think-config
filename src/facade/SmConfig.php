<?php
namespace stevema\afs\facade;

use think\Facade;

/**
 * Class Captcha
 * @package think\captcha\facade
 * @mixin \stevema\afs\AfsManage
 */
class SmConfig extends Facade
{
    protected static function getFacadeClass()
    {
        return \stevema\config\ConfigManage::class;
    }
}