<?php
// +----------------------------------------------------------------------
// | Config 配置文件
// +----------------------------------------------------------------------

return [
    // 对应的模型  id、source、type、name、value
    'model' => env('SMCONFIG_MODEL', \app\model\ConfigModel::class),
    // 默认配置
    'default' => [
        'type1' => [
            'name1' => 'value1',
            'name2' => 'value2',
        ],
    ]
];