<?php
use stevema\config\ConfigManage;
if (!function_exists('smconfig')) {
    function smconfig(): ConfigManage{
        if(function_exists('app')) {
            return app('smconfig');
        }
        return new ConfigManage();
    };
}
if (!function_exists('smconfig_get')) {
    function smconfig_get(string $type, string $name = null, $defaultValue = null, mixed $source = 'default'){
        return smconfig()->get($type,$name,$defaultValue,$source);
    };
}
if (!function_exists('smconfig_set')) {
    function smconfig_set(string $type, string $name = null, mixed $value = '', mixed $source = 'default'){
        return smconfig()->set($type, $name, $value, $source);
    };
}