<?php

use think\migration\Migrator;
use think\migration\db\Column;

class Config extends Migrator
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table  =  $this->table('configs', array('comment' => '配置表'));
        $table->addColumn('source', 'string',array('limit'  =>  50,'default'=>'','comment'=>'源'))
            ->addColumn('type', 'string',array('limit'  =>  50,'default'=>'','comment'=>'类型'))
            ->addColumn('name', 'string',array('limit'  =>  50,'default'=>'','comment'=>'键'))
            ->addColumn('value', 'text',array('default' => null,'comment' => '值'))
            ->addTimestamps()
            ->create();
    }
}
