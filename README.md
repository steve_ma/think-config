# think-config

#### 介绍
自定义一个配置服务[缓存-数据库-默认配置]
环境: ThinkPHP:8.0 PHP:8.1 理论上 php >= 7.1 thinkPHP >= 6.0 都可以使用
### 安装教程

```shell
# 安装
$ composer require stevema/think-config
```

### 使用说明

```php
# 1、先有表-创建表-表结构如下
CREATE TABLE `sm_configs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `source` varchar(50) DEFAULT NULL COMMENT '源',
  `type` varchar(50) NOT NULL DEFAULT '' COMMENT '类型',
  `name` varchar(50) NOT NULL DEFAULT '' COMMENT '键',
  `value` text NOT NULL COMMENT '值',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_time` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COMMENT='配置表';
# 也可以使用migrate - - 文件我放在database里了 有需要的可以去找一下
# 2、创建对应的模型(Model) 
$ php think make:model Config

# 3、修改配置文件 /config/smconfig.php 或在 /.env 文件中添加配置

#SMCONFIG
SMCONFIG_MODEL="app\model\ConfigModel"

```

### 目录说明

```
├─ src
│   ├─ config           # 配置文件目录
│   │   └─ smconfig.php # 配置文件
│   ├─ database         # 数据库目录
│   │   └─ 20230807053657_config.php      # 数据库迁移文件  config表
│   ├─ facades          # 门面文件目录
│   │   └─ SmConfig.php # 门面
│   └─ ConfigManage.php    # 项目主文件
│   └─ ConfigService.php   # TP使用的Service文件
│   └─ helper.php       # 帮助文件
└─ composer.json        # 配置

```

### 使用方式

```php
# 引用门面
use stevema\config\facades\SmConfig;

# 随便加一个路由
Route::get('/t/afs', function(){
    # 定义 scene 和 sessionId
    SmConfig::set("type1","name1","value1","defaultSource");
    SmConfig::get("type1","name1","defaultValue","defaultSource");
    
    # 第二种方式 来自 app->bind('smconfig', Stevema\Afs\AfsManage::class)
    # 这里的 smconfig 来自配置文件  service_name
    $smconfig = app('smconfig');
    $smconfig->set("type1","name1","value1","defaultSource");
    $value = $smconfig->get("type1","name1","defaultValue","defaultSource");
    
    # 第三种方式  来自 helper 
    $smconfig = smconfig();
    $smconfig->set("type1","name1","value1","defaultSource");
    $value = $smconfig->get("type1","name1","defaultValue","defaultSource");
    # 或者
    smconfig_set("type1","name1","value1","defaultSource");
    $value = smconfig_get("type1","name1","defaultValue","defaultSource");
});
```

### 备注

